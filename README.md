# MuCCC API

To run install poetry and execute:

    $ poetry run muccc-api

If you're using Nix, you can use `nix-build` to build a package with all dependencies.
