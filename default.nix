{ pkgs ? import <nixpkgs> {} }:

pkgs.poetry2nix.mkPoetryApplication {
  projectDir = ./.;
  python = pkgs.python39;
  overrides = pkgs.poetry2nix.overrides.withDefaults (self: super: {
    wiki-kalenderscraper = super.wiki-kalenderscraper.overrideAttrs (old: {
      nativeBuildInputs = old.nativeBuildInputs ++ [ self.poetry ];
    });
  });
}
