import pydle
import aiohttp
import asyncio
import os
from loguru import logger

from .FlipdotAPI.FlipdotMatrix import FlipdotMatrix
from .log import setup_logging
from .schleuse import wait_for_schleuse_state

flipdotMatrix = FlipdotMatrix()

IRC_HOST = os.getenv("IRC_HOST", "jmt1.darkfasel.net")
CHANNEL = os.getenv("CHANNEL", "#ccc-test")
NICKNAME = os.getenv("NICKNAME", "schleuse")


class SchleuseClient(pydle.Client):
    schleuse_state = None
    schleuse_update_task = None

    async def on_connect(self):
        await super().on_connect()
        logger.info("connected to irc")
        if self.schleuse_update_task is None:
            self.schleuse_update_task = asyncio.create_task(
                wait_for_schleuse_state(self.update_topic)
            )
        await self.join(CHANNEL)

    async def on_topic_change(self, channel, message, by):
        if channel == CHANNEL and not self.is_same_nick(self.nickname, by):
            await self.update_topic()

    async def on_join(self, channel, nick):
        if self.is_same_nick(self.nickname, nick):
            logger.info(f"joined {channel}")

    async def on_raw_396(self, message):
        pass

    async def on_raw_329(self, message):
        await super().on_raw_329(message)
        await self.update_topic()

    async def update_topic(self, new_state=None):
        if new_state is not None:
            self.schleuse_state = new_state

        if CHANNEL not in self.channels.keys() or self.schleuse_state is None:
            return

        current = self.channels[CHANNEL]["topic"]
        if current is None:  # no topic set
            current = ""

        comp = current.split(" | ")
        rest = " | ".join(comp[1:])
        optsep = " | " if len(rest) > 0 else ""
        new = f"club {self.schleuse_state}{optsep}{rest}"
        if current != new:
            logger.info(f"update_topic: {current} ==> {new}")
            await self.set_topic(CHANNEL, new)


def run():
    setup_logging()
    client = SchleuseClient(NICKNAME)
    client.run(IRC_HOST, tls=True)
