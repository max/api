from fastapi import FastAPI, WebSocket, WebSocketDisconnect
from fastapi.responses import PlainTextResponse, RedirectResponse, Response
from fastapi.middleware.cors import CORSMiddleware
from fastapi.openapi.utils import get_openapi
from fastapi.staticfiles import StaticFiles
from prometheus_fastapi_instrumentator import Instrumentator as PrometheusInstrumentator
from loguru import logger
import uvicorn
import asyncio
import os

from .schleuse import SchleuseUdpReceiver, SchleuseStateEnum, SchleuseStateResponse
from .wiki_kalenderscraper import WikiKalenderscraper
from .spaceapi import SpaceAPIResponse, SpaceAPIState
from .log import setup_logging

app = FastAPI()

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=False,
    allow_methods=["*"],
    allow_headers=["*"],
    max_age=3600 * 24,
)

PrometheusInstrumentator().instrument(app).expose(app)

app.mount(
    "/static",
    StaticFiles(directory=os.path.join(os.path.dirname(__file__), "static")),
    name="static",
)


def custom_openapi():
    if app.openapi_schema:
        return app.openapi_schema
    openapi_schema = get_openapi(
        title="MuCCC API",
        version="0.1.0-dev",
        description="API for the MuCCC hackerspace.",
        routes=app.routes,
    )
    openapi_schema["info"]["x-logo"] = {
        "url": "https://api.muc.ccc.de/static/logo.png",
    }
    openapi_schema["externalDocs"] = {"url": "https://wiki.muc.ccc.de/api"}
    app.openapi_schema = openapi_schema
    return app.openapi_schema


app.openapi = custom_openapi  # type: ignore

schleuse = SchleuseUdpReceiver()
wikikalenderscraper = WikiKalenderscraper()


@app.on_event("startup")
async def run_tasks():
    asyncio.create_task(schleuse.run())
    asyncio.create_task(wikikalenderscraper.run())


@app.get("/", include_in_schema=False)
async def root():
    return RedirectResponse("/docs", status_code=302)


@app.get(
    "/spaceapi.json",
    summary="Get SpaceAPI data",
    tags=["spaceapi"],
    response_model=SpaceAPIResponse,
)
async def spaceapi():
    return SpaceAPIResponse(
        state=SpaceAPIState(
            open=None if schleuse.state is None else schleuse.state == "public",
            message=schleuse.state or "not available",
        )
    )


@app.get(
    "/schleuse.json",
    summary="Return the state of the Luftschleuse",
    tags=["schleuse"],
    response_model=SchleuseStateResponse,
)
async def hqstate(poll: bool = False):
    """
    Returns the state of the Luftschleuse as a JSON string.

    If parameter `poll` is true, this will make the request block indefinitely
    until the state changes and return the new state.
    """
    if poll:
        await schleuse.wait_for_state_change()
    return {"state": schleuse.state}


@app.get(
    "/wiki_kalender.ics",
    summary="All events as ical feed",
    tags=["calendar"],
)
async def calendar_ics():
    """
    All events of the current year as ical feed
    """
    return Response(content=wikikalenderscraper.ical, media_type="text/calendar")


@app.get(
    "/wiki_kalender_public.ics",
    summary="Public Events as ical feed",
    tags=["calendar"],
)
async def calendar_public_ics():
    """
    All public events of the current year as ical feed
    """
    return Response(content=wikikalenderscraper.ical_public, media_type="text/calendar")


@app.get(
    "/next_event.json",
    summary="Legacy: Next event as JSON",
    tags=["calendar"],
)
async def next_event():
    """
    Legacy next event as weird JSON for anzeigr in Hauptraum
    """
    return wikikalenderscraper.next_event_json


@app.websocket("/schleuse.json/ws")
async def hqstate_ws(websocket: WebSocket):
    async def notify_websocket():
        while True:
            await websocket.send_json(await hqstate())
            await schleuse.wait_for_state_change()

    await websocket.accept()
    notify_task = asyncio.create_task(notify_websocket())

    try:
        async for msg in websocket.iter_text():
            pass  # Ignore incoming messages but react to disconnects
    except WebSocketDisconnect:
        websocket.close()

    notify_task.cancel()


def run():
    server = uvicorn.Server(uvicorn.Config(app, host="::", port=8020))
    setup_logging()
    server.run()
