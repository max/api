import asyncio
from loguru import logger

from .FlipdotAPI.FlipdotMatrix import FlipdotMatrix
from .log import setup_logging
from .schleuse import SchleuseStateEnum, wait_for_schleuse_state

flipdotMatrix = FlipdotMatrix()


async def update(state: SchleuseStateEnum):
    flipdotMatrix.showText("\x01 state    " + state, linebreak=True, xPos=2, yPos=1)
    logger.info("New state: " + state)


def run():
    setup_logging()
    asyncio.run(wait_for_schleuse_state(update))
