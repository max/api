from pydantic import BaseModel
from typing import Optional, List


class SpaceAPIState(BaseModel):
    open: Optional[bool]
    message: str


class SpaceAPIResponse(BaseModel):
    api: str = "0.13"
    api_compatibility: List[str] = ["14"]
    space: str = "MuCCC"
    logo: str = "https://muc.ccc.de/lib/tpl/muc3/images/muc3_klein.gif"
    url: str = "https://muc.ccc.de"
    location = {
        "address": "Heßstrasse 90, München, Germany",
        "lat": 48.15367,
        "lon": 11.56078,
        "timezone": "Europe/Berlin",
    }
    contact = {
        "ml": "talk@lists.muc.ccc.de",
        "twitter": "@muccc",
        "irc": "ircs://irc.darkfasel.net/#ccc",
        "email": "info@muc.ccc.de",
        "mumble": "mumble://fasel.muc.ccc.de",
        "matrix": "#ccc:darkfasel.net",
    }
    issue_report_channels: List[str] = ["ml"]
    feeds = {
        "calendar": {
            "url": "https://api.muc.ccc.de/wiki_kalender_public.ics",
            "type": "ical",
        },
    }
    projects: List[str] = [
        "https://wiki.muc.ccc.de/projekte",
        "https://github.com/muccc",
        "https://gitlab.muc.ccc.de",
    ]
    state: SpaceAPIState
