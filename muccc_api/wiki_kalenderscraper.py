from anyio import sleep
import wiki_kalenderscraper


class WikiKalenderscraper:
    ical: str
    ical_public: str
    next_event_json: str

    def __init__(self):
        self.scraper = wiki_kalenderscraper.KalenderScraper()
        self._update()

    def _update(self):
        self.ical = self.scraper.cal.to_ical()
        self.ical_public = self.scraper.cal_public.to_ical()
        self.next_event_json = self.scraper.next_event_json()

    async def run(self):
        while True:
            self.scraper.scrape()
            self._update()
            await sleep(30)
