from anyio import create_udp_socket, Condition
from enum import Enum
from pydantic import BaseModel
from typing import Optional
from loguru import logger
import asyncio
import aiohttp
import os
import socket


class SchleuseStateEnum(str, Enum):
    down = "down"
    closed = "closed"
    member = "member"
    public = "public"


class SchleuseStateResponse(BaseModel):
    state: Optional[SchleuseStateEnum]


class SchleuseUdpReceiver:
    state: Optional[SchleuseStateEnum]

    def __init__(self):
        self.state = None
        self._state_changed = Condition()

    async def wait_for_state_change(self):
        async with self._state_changed:
            await self._state_changed.wait()

    async def run(self):
        async with await create_udp_socket(
            family=socket.AF_INET6, local_host="::", local_port=2080
        ) as udp:
            async for packet, (host, port) in udp:
                logger.debug(f"UDP datagram from {host}:{port} => {packet}")
                newstate = packet.decode("ascii").strip()
                if newstate != self.state:
                    self.state = newstate
                    logger.info(f"schleuse.state: {self.state}")
                    async with self._state_changed:
                        self._state_changed.notify_all()


async def wait_for_schleuse_state(
    callback, api_base_url=os.getenv("MUCCC_API_BASE_URL", "http://localhost:8020")
):
    async with aiohttp.ClientSession() as session:
        while True:
            try:
                async with session.ws_connect(api_base_url + "/schleuse.json/ws") as ws:
                    logger.info("connected to websocket")
                    async for msg in ws:
                        if msg.type == aiohttp.WSMsgType.TEXT:
                            state = msg.json()["state"]
                            if state is not None:
                                asyncio.create_task(callback(state))
                        elif msg.type == aiohttp.WSMsgType.ERROR:
                            break
                logger.warning("disconnected from websocket")
            except aiohttp.client_exceptions.ClientConnectorError:
                logger.error("can't connect to websocket")
            except aiohttp.client_exceptions.WSServerHandshakeError:
                logger.error("handshake error with websocket")
            except Exception as e:
                logger.error(f"unknown exception: {e}")
            finally:
                await asyncio.sleep(5)
