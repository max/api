{
  description = "MuCCC API";

  inputs.nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
  inputs.flake-utils.url = "github:numtide/flake-utils";
  inputs.poetry2nix = {
    url = "github:nix-community/poetry2nix";
    inputs.nixpkgs.follows = "nixpkgs";
    inputs.flake-utils.follows = "flake-utils";
  };

  outputs = { self, nixpkgs, flake-utils, poetry2nix, ... }: rec {
    overlay = final: prev: {
      muccc-api = import ./default.nix { pkgs = final; };
    };
  } // (flake-utils.lib.eachDefaultSystem (system:
    let
      pkgs = import nixpkgs {
        inherit system;
        overlays = [ self.overlay poetry2nix.overlay ];
      };
    in
    rec {
      defaultPackage = self.packages.${system}.muccc-api;

      packages.muccc-api = pkgs.muccc-api;

      devShell = pkgs.mkShell {
        inputsFrom = [ defaultPackage ];
        packages = with pkgs; [ defaultPackage.dependencyEnv poetry ];
      };
    }
  ));
}
